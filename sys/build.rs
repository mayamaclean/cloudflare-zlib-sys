extern crate cc;

use std::env;
use std::path::PathBuf;
use std::process;

fn main() {
    let mut cc = cc::Build::new();
    let comp = cc.get_compiler();
    let msvc_bugs = comp.is_like_msvc();
    cc.warnings(false);
    cc.pic(true);

    let target_pointer_width = env::var("CARGO_CFG_TARGET_POINTER_WIDTH").expect("CARGO_CFG_TARGET_POINTER_WIDTH var");
    let target_arch = env::var("CARGO_CFG_TARGET_ARCH").expect("CARGO_CFG_TARGET_ARCH var");

    if let Ok(target_cpu) = env::var("TARGET_CPU") {
        cc.flag_if_supported(&format!("-march={}", target_cpu));
    }

    let vendor = PathBuf::from(env::var_os("CARGO_MANIFEST_DIR").unwrap()).join("vendor");
    println!("cargo:include={}", vendor.display());
    cc.include(&vendor);

    // git submodule sadness
    if !vendor.join("zlib.h").exists() {
        process::Command::new("git").arg("submodule").arg("init").status().expect("git");
        process::Command::new("git").arg("submodule").arg("update").status().expect("git");
    }

    if "32" != target_pointer_width && !msvc_bugs {
        cc.define("HAVE_OFF64_T", Some("1"));
        cc.define("_LARGEFILE64_SOURCE", Some("1"));
    }

    if msvc_bugs {
        cc.define("_CRT_SECURE_NO_DEPRECATE", Some("1"));
        cc.define("_CRT_NONSTDC_NO_DEPRECATE", Some("1"));
        cc.flag_if_supported("/arch:AVX");
    } else {
        cc.flag_if_supported("-msse4.2");
        cc.define("HAVE_UNISTD_H", Some("1"));
    }

    if cfg!(feature = "asm") {
        if target_arch == "x86_64" && msvc_bugs {
            cc.define("__x86_64__", Some("1"));
        }

        if target_arch == "aarch64" {
            cc.flag_if_supported("-march=armv8-a+crc");
        } else {
            // msvc doesn't have a flag for this
            if !msvc_bugs {
                if cc.is_flag_supported("-mpclmul").is_ok() {
                    cc.flag("-mpclmul");
                    cc.define("HAS_PCLMUL", None);
                    cc.file(vendor.join("contrib/amd64/crc32-pclmul_asm.S"));
                }
            }

            let sse_flag = if msvc_bugs {"/arch:AVX"} else {"-msse4.2"};
            if cc.is_flag_supported(sse_flag).is_ok() {
                cc.flag(sse_flag);
                cc.define("HAS_SSE42", None);
            }
        }

        if !msvc_bugs {
            cc.define("HAVE_HIDDEN", Some("1"));
        }
    }

    cc.file(vendor.join("adler32.c"));
    cc.file(vendor.join("compress.c"));
    cc.file(vendor.join("crc32.c"));
    cc.file(vendor.join("deflate.c"));
    cc.file(vendor.join("gzclose.c"));
    cc.file(vendor.join("gzlib.c"));
    cc.file(vendor.join("gzread.c"));
    cc.file(vendor.join("gzwrite.c"));
    cc.file(vendor.join("infback.c"));
    cc.file(vendor.join("inffast.c"));
    cc.file(vendor.join("inflate.c"));
    cc.file(vendor.join("inftrees.c"));
    cc.file(vendor.join("trees.c"));
    cc.file(vendor.join("uncompr.c"));
    cc.file(vendor.join("zutil.c"));

    cc.compile("cloudflare_zlib");
}

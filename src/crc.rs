use cloudflare_zlib_sys::*;

#[derive(Copy, Clone)]
pub struct CfCrc {
    pub val: uLong,
    pub len: z_off_t,
}

impl CfCrc {
    pub fn default() -> Self {
        CfCrc {
            val: 0,
            len: 0,
        }
    }

    pub fn new_from(buf: &[u8]) -> Self {
        unsafe {
            CfCrc {
                val: crc32(0, buf.as_ptr(), buf.len() as uInt),
                len: buf.len() as z_off_t,
            }
        }
    }

    pub fn update(&mut self, buf: &[u8]) {
        unsafe {
            let old = self.val;
            self.val = crc32(old, buf.as_ptr(), buf.len() as uInt);
            self.len += buf.len() as z_off_t;
        }
    }

    pub fn combine(&mut self, other: &CfCrc) {
        unsafe {
            let old = self.val;
            self.val = crc32_combine(old, other.val, other.len);
            self.len += other.len;
        }
    }

    pub fn inner(&self) -> u64 {
        self.val as u64
    }
}

impl From<&[u8]> for CfCrc {
    fn from(buf: &[u8]) -> CfCrc {
        unsafe {
            CfCrc {
                val: crc32(0, buf.as_ptr(), buf.len() as u32),
                len: buf.len() as z_off_t,
            }
        }
    }
}

impl From<&Vec<u8>> for CfCrc {
    fn from(buf: &Vec<u8>) -> CfCrc {
        CfCrc::new_from(buf)
    }
}

impl std::ops::Deref for CfCrc {
    type Target = uLong;

    fn deref(&self) -> &uLong {
        &self.val
    }
}

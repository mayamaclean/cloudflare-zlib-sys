use ZError;
use cloudflare_zlib_sys;
use cloudflare_zlib_sys::*;
use std::mem;
use std::os::raw::*;

pub struct Inflate {
    stream: z_stream,
}

impl Drop for Inflate {
    fn drop(&mut self) {
        unsafe {
            inflateEnd(&mut self.stream);
        }
    }
}

/// Decompress data to `Vec`
pub fn inflate(data: &[u8]) -> Result<Vec<u8>, ZError> {
    let inf = Inflate::new()?;
    inf.inflate(data)
}

impl Inflate {
    pub fn new() -> Result<Self, ZError> {
        if !::is_supported() {
            return Err(ZError::IncompatibleCPU);
        }
        unsafe {
            let mut stream = mem::zeroed();
            let res = inflateInit(&mut stream);
            if Z_OK != res {
                return Err(ZError::new(res));
            }
            Ok(Self {
                stream,
            })
        }
    }

    pub fn new_with_window(bits: c_int) -> Result<Self, ZError> {
        if !::is_supported() {
            return Err(ZError::IncompatibleCPU);
        }
        unsafe {
            let mut stream = mem::zeroed();
            let res = inflateInit2(&mut stream, bits);
            if Z_OK != res {
                return Err(ZError::new(res));
            }
            Ok(Self {
                stream,
            })
        }
    }

    pub fn set_dict(&mut self, dict: &[u8]) -> Result<(), ZError> {
        unsafe {
            match inflateSetDictionary(&mut self.stream as *mut _, dict[0] as *const _, dict.len() as u32) {
                Z_OK => Ok(()),
                othr => Err(ZError::new(othr)),
            }
        }
    }

    pub fn inflate(mut self, inp: &[u8]) -> Result<Vec<u8>, ZError> {
        self.stream.next_in = inp.as_ptr() as *mut _;
        self.stream.avail_in = inp.len() as uInt;
        let mut buf = Vec::with_capacity(inp.len() + inp.len()/2);
        loop {
            unsafe {
                let len = buf.len();
                self.stream.next_out = buf[len..].as_mut_ptr();
                self.stream.avail_out = (buf.capacity() - buf.len()) as uInt;

                let total_out_before = self.stream.total_out as usize;
                let res = cloudflare_zlib_sys::inflate(&mut self.stream, Z_FINISH);

                // extend the vec length by number of bytes written by zlib
                let total_out_written = self.stream.total_out as usize;
                if total_out_written > total_out_before {
                    buf.set_len(len + total_out_written - total_out_before);
                } else {
                    debug_assert_eq!(total_out_before, self.stream.total_out as usize);
                }

                match res {
                    Z_STREAM_END => {
                        return Ok(buf);
                    }
                    Z_OK | Z_BUF_ERROR => {
                        let reserve = buf.len().max(1<<16);
                        buf.reserve(reserve);
                    },
                    other => {
                        return Err(ZError::new(other));
                    }
                }
            }
        }
    }

    pub fn inflate_sync(&mut self, inp: &[u8], buf: &mut Vec<u8>) -> Result<(), ZError> {
        self.stream.next_in = inp.as_ptr() as *mut _;
        self.stream.avail_in = inp.len() as uInt;

        loop {
            unsafe {
                let len = buf.len();
                self.stream.next_out = buf[len..].as_mut_ptr();
                self.stream.avail_out = (buf.capacity() - buf.len()) as uInt;

                let total_out_before = self.stream.total_out as usize;
                let res = cloudflare_zlib_sys::inflate(&mut self.stream, Z_SYNC_FLUSH);

                // extend the vec length by number of bytes written by zlib
                let total_out_written = self.stream.total_out as usize;
                if total_out_written > total_out_before {
                    buf.set_len(len + total_out_written - total_out_before);
                } else {
                    debug_assert_eq!(total_out_before, self.stream.total_out as usize);
                }

                match res {
                    Z_STREAM_END => {
                        return Ok(());
                    }
                    Z_OK | Z_BUF_ERROR => {
                        if self.stream.avail_in == 0 {
                            return Ok(())
                        }
                        let reserve = buf.len().max(1<<16);
                        buf.reserve(reserve);
                    },
                    other => {
                        return Err(ZError::new(other));
                    }
                }
            }
        }
    }
}
